%define module r8168
%define pkgname realtek-r8168

Summary: Reaktek r8168 GbE kernel module (dkms)
Name: %{pkgname}-dkms
Version: 8.048.03
# Release Start
Release: 1%{?dist}
# Release End
License: GPLv2+
URL: https://www.realtek.com/en/component/zoo/category/network-interface-controllers-10-100-1000m-gigabit-ethernet-pci-express-software
Vendor: Realtek
Group: System Environment/Kernel
BuildArch: noarch
Requires: dkms bash binutils gcc kmod kernel-devel
Source: %{pkgname}-%{version}.tar.bz2
Provides: kmod-r8168 = %{version} kmod(r8168.ko) = %{version}

%description
A dkms module for the Realtek r8168 GBE Ethernet driver.

%prep
%setup -n r8168-%{version} -q

%build

%install
mkdir -p %{buildroot}/usr/src/%{module}-%{version}/
cp -r src/. %{buildroot}/usr/src/%{module}-%{version}
cat <<'EOF' > %{buildroot}/usr/src/%{module}-%{version}/dkms.conf
PACKAGE_NAME="%{pkgname}"
PACKAGE_VERSION="%{version}"
MAKE="'make' -j`nproc` KVER=${kernelver} KERNELDIR=${kernel_source_dir} BASEDIR=${kernel_source_dir}/.. modules"
CLEAN="make clean"
BUILT_MODULE_NAME[0]="%{module}"
BUILT_MODULE_LOCATION[0]=""
DEST_MODULE_LOCATION[0]="/kernel/drivers/net/ethernet/realtek"
AUTOINSTALL="yes"
REMAKE_INITRD=no
EOF

%clean
rm -rf %{buildroot}

%files
%defattr(0644,root,root)
%attr(0755,root,root) /usr/src/%{module}-%{version}/

%post
if ! /usr/sbin/dkms status %{module}/%{version} | grep -q added
then
    /usr/sbin/dkms add -m %{module} -v %{version}
fi
/usr/sbin/dkms build -m %{module} -v %{version}
/usr/sbin/dkms install -m %{module} -v %{version}
exit 0

%preun
/usr/sbin/dkms remove -m %{module} -v %{version} --all
exit 0

%changelog
* Tue Apr 06 2021 Thomas Juberg <thomas.juberg@gmail.com> 8.048.03-1dkms
- Initial version
